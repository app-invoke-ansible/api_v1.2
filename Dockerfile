FROM registry.access.redhat.com/ubi8/openjdk-17:1.18

WORKDIR /deployments

# Copiar el ejecutable nativo generado por Quarkus
COPY target/*-runner /deployments/quarkus-app

# Configuración del contenedor para ejecutar el binario nativo
ENTRYPOINT ["./quarkus-app"]