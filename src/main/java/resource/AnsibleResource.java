package resource;

import exceptions.AnsibleServiceException;
//import io.quarkus.security.Authenticated;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import services.AnsibleService;

@Path("/api") // Ruta base para todos los endpoints en esta clase
@Transactional
//@Authenticated
public class AnsibleResource {

    private final AnsibleService ansibleService; // Cambiar a final

    // Constructor que acepta AnsibleService como parámetro
    @Inject
    public AnsibleResource(AnsibleService ansibleService) {
        this.ansibleService = ansibleService; // Inyección de la dependencia
    }

    // Endpoint para obtener los detalles de un job template
    @GET
    @Path("/job_templates/{id}")
    public Response getJobTemplateDetails(@PathParam("id") String jobTemplateId) {
        try {
            String details = ansibleService.getJobTemplateDetails(jobTemplateId);
            return Response.ok(details).build();
        } catch (AnsibleServiceException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(e.getMessage())
                    .build();
        }
    }

    @POST
    @Path("/job_template/{id}/execute")
    @Produces(MediaType.APPLICATION_JSON)
    public Response executeJobTempale(@PathParam("id") String jobTemplateId) {
        try {
            String response = ansibleService.executeJobTemplate(jobTemplateId);
            return Response.ok(response).build(); // Devuelve la respuesta con estado 200
        } catch (AnsibleServiceException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error al ejecutar el job template: " + e.getMessage())
                    .build(); // Devuelve un error 500 en caso de excepción
        }
    }
}