package resource;

import dto.EjecucionDTO;
import services.EjecucionService;
import services.AnsibleService;
import exceptions.AnsibleServiceException;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;

@Path("/ejecuciones")
public class EjecucionController {

    private final EjecucionService ejecucionService;
    private final AnsibleService ansibleService;

    public EjecucionController(EjecucionService ejecucionService, AnsibleService ansibleService) {
        this.ejecucionService = ejecucionService;
        this.ansibleService = ansibleService;
    }

    @GET
    @Path("/ultimas")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EjecucionDTO> obtenerUltimasEjecuciones() {
        return ejecucionService.obtenerUltimasEjecuciones();
    }

    @GET
    @Path("/{jobId}/log")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getLogEjecucion(@PathParam("jobId") String jobId) {
        try {
            String log = ansibleService.getJobOutput(jobId);
            return Response.ok(log).build();
        } catch (AnsibleServiceException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error al obtener el log de la ejecución: " + e.getMessage())
                    .build();
        }
    }
}
