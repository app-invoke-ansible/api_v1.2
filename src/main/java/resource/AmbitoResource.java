package resource;

import java.util.List;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import repositories.AmbitoRepository;
import entities.Ambito;

@Path("/ambitos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AmbitoResource {

    private final AmbitoRepository ambitoRepository;

    @Inject
    public AmbitoResource(AmbitoRepository ambitoRepository) {
        this.ambitoRepository = new AmbitoRepository();
    }

    @GET
    @Transactional
    public List<Ambito> getAllAmbitos() {
        return ambitoRepository.listAll();  // Llama al servicio para obtener todos los ámbitos
    }
}