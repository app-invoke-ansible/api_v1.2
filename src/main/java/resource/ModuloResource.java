package resource;

import java.io.IOException;
import java.util.List;

import dto.ModuloEjecucionesDTO;
import entities.Modulo;
import exceptions.AnsibleServiceException;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import repositories.ModuloRepository;
import services.EjecucionService;
import services.ModuloService;

@Path("/modulos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ModuloResource {

    private final ModuloService moduloService;
    private final ModuloRepository moduloRepository;
    private final EjecucionService ejecucionService;

    @Inject
    public ModuloResource(ModuloService moduloService, ModuloRepository moduloRepository, EjecucionService ejecucionService) {
        this.moduloService = moduloService;
        this.moduloRepository = moduloRepository;
        this.ejecucionService = ejecucionService;
    }

    @GET
    @Transactional
    public List<Modulo> getAllModulos() {
        return moduloRepository.listAll();
    }

    @GET
    @Path("/{id}")
    @Transactional
    public Response getModuloById(@PathParam("id") Long id) {
        Modulo modulo = moduloRepository.findById(id);
        if (modulo == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("Módulo no encontrado con ID: " + id)
                    .build();
        }
        return Response.ok(modulo).build();
    }

    @POST
    @Path("/save/{id}")
    public Response automateSaveModulo(@PathParam("id") String jobTemplateId) {
        try {
            moduloService.automateModuloSaving(jobTemplateId);
            return Response.ok().build();
        } catch (IOException | AnsibleServiceException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error al automatizar el guardado del módulo: " + e.getMessage())
                    .build();
        }
    }

    @POST
    @Path("/{id}/execute")
    @Transactional
    public Response executeModulo(@PathParam("id") Long moduloId) {
        try {
            moduloService.executeModulo(moduloId);
            return Response.ok("Módulo ejecutado con éxito").build();
        } catch (AnsibleServiceException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error al ejecutar el módulo: " + e.getMessage())
                    .build();
        } catch (IOException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error al procesar la respuesta: " + e.getMessage())
                    .build();
        }
    }

    @POST
    @Path("/ejecuciones/{jobId}/cancelar")
    @Transactional
    public Response cancelarEjecucion(@PathParam("jobId") String jobId) {
        try {
            moduloService.stopModuloExecution(jobId);
            return Response.ok("Ejecución cancelada exitosamente.").build();
        } catch (AnsibleServiceException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity("Error al cancelar la ejecución: " + e.getMessage())
                    .build();
        }
    }

    // Endpoint para obtener las ejecuciones de un módulo específico
    @GET
    @Path("/{moduloId}/ejecuciones")
    @Transactional
    @Produces(MediaType.APPLICATION_JSON)
    public Response getEjecucionesPorModulo(@PathParam("moduloId") Long moduloId) {
        try {
            ModuloEjecucionesDTO ejecuciones = ejecucionService.getEjecucionesPorModuloId(moduloId);
            return Response.ok(ejecuciones).build();
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity("Módulo no encontrado con ID: " + moduloId)
                    .build();
        }
    }
}
