package resource;

import java.util.List;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import services.CategoriaService;
import entities.Categoria;

@Path("/categorias")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CategoriaResource {

    private final CategoriaService categoriaService;

    @Inject
    public CategoriaResource(CategoriaService categoriaService) {
        this.categoriaService = categoriaService;
    }

    @GET
    @Transactional
    public List<Categoria> getAllCategorias() {
        return categoriaService.getAllCategorias();
    }

    @GET
    @Path("/{id}")
    @Transactional
    public Response getCategoriasByCategoriaId(@PathParam("id") Long id) {
        return categoriaService.getCategoriasByCategoriaId(id)
                .map(categorias -> Response.ok(categorias).build())
                .orElse(Response.status(Response.Status.NOT_FOUND)
                        .entity("Categoría no encontrada con ID: " + id)
                        .build());
    }

    @POST
    @Transactional
    public Response createCategoria(Categoria categoria) {
        Categoria nuevaCategoria = categoriaService.createCategoria(categoria);
        return Response.ok(nuevaCategoria).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response updateCategoria(@PathParam("id") Long id, Categoria categoriaDetails) {
        return Response.ok(categoriaService.updateCategoria(id, categoriaDetails)).build();
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response deleteCategoria(@PathParam("id") Long id) {
        categoriaService.deleteCategoria(id);
        return Response.noContent().build();
    }
}
