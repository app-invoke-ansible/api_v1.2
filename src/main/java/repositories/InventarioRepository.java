package repositories;

import entities.Inventario;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class InventarioRepository implements PanacheRepository<Inventario> {

    // Método para encontrar un inventario por su nombre
    public Inventario findByNombre(String nombreInventario) {
        return find("nombreInventario", nombreInventario).firstResult();
    }

}
