package repositories;

import entities.Ejecucion;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class EjecucionRepository implements PanacheRepository<Ejecucion> {
    public List<Ejecucion> findTop10ByOrderByStartTimeDesc() {
        return find("ORDER BY startTime DESC").page(0, 10).list();
    }
}