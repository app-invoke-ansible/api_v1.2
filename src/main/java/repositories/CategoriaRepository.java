package repositories;

import entities.Ambito;
import entities.Categoria;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;
import org.hibernate.Hibernate; // Importa Hibernate aquí

import java.util.List;

@ApplicationScoped
public class CategoriaRepository implements PanacheRepository<Categoria> {

    // Método para buscar categorías por nombre y ambito
    public Categoria findByNameAndAmbito(String name, Ambito ambito) {
        return find("name = ?1 and ambito = ?2", name, ambito).firstResult();
    }

    public List<Categoria> findByAmbitoId(Long ambitoId) {
        return list("ambito.id", ambitoId);
    }

    // Método para buscar una categoría por ID, incluyendo las relaciones
    public Categoria findByIdWithRelations(Long id) {
        Categoria categoria = findById(id);
        if (categoria != null) {
            // Inicializa las relaciones perezosas (Ambito y Modulos)
            Hibernate.initialize(categoria.getAmbito());
            Hibernate.initialize(categoria.getModuloList());
        }
        return categoria;
    }
}
