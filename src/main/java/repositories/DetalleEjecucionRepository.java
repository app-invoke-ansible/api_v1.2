package repositories;

import entities.DetalleEjecucion;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class DetalleEjecucionRepository implements PanacheRepository<DetalleEjecucion> {
    
}