package repositories;

import entities.Ambito;
import io.quarkus.hibernate.orm.panache.PanacheRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class AmbitoRepository implements PanacheRepository<Ambito> {

    public Ambito findByName(String name) {
        return find("name", name).firstResult();
    }
}
