package entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "detalle_ejecuciones")
public class DetalleEjecucion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Cambiar a una relación muchos a uno
    @ManyToOne
    @JoinColumn(name = "ejecucion_id", nullable = false)
    private Ejecucion ejecucion;

    @Lob
    private String output;

    @Lob
    private String errorDetails;

    @NotNull(message = "El host no debe ser NULO")
    @NotEmpty(message = "El host no puede ir VACIO")
    private String hostMaquina;

    @NotNull(message = "El nombre no debe ser NULO")
    @NotEmpty(message = "El nombre no puede ir VACIO")
    private String nombreMaquina;

    // Getters y setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ejecucion getEjecucion() {
        return ejecucion;
    }

    public void setEjecucion(Ejecucion ejecucion) {
        this.ejecucion = ejecucion;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(String errorDetails) {
        this.errorDetails = errorDetails;
    }

    public String getHostMaquina() {
        return hostMaquina;
    }

    public void setHostMaquina(String hostMaquina) {
        this.hostMaquina = hostMaquina;
    }

    public String getNombreMaquina() {
        return nombreMaquina;
    }

    public void setNombreMaquina(String nombreMaquina) {
        this.nombreMaquina = nombreMaquina;
    }
    
}