package entities;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Modulo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    @Column(columnDefinition = "TEXT")
    private String description;
    private String playbook;
    private String jobType;
    private String status;
    private String lastJobRun;
    private boolean lastJobFailed;
    private String jobTemplateId;
    @ManyToOne
    @JoinColumn(name = "categoria_id")
    private Categoria categoria;
    private String duracion;

    @ManyToMany
    @JoinTable(name = "modulo_inventario", joinColumns = @JoinColumn(name = "modulo_id"), inverseJoinColumns = @JoinColumn(name = "inventario_id"))
    @JsonIgnore // Ignora esta propiedad durante la serialización
    private List<Inventario> inventarios = new ArrayList<>(); // Inicialización aquí

    @OneToMany(mappedBy = "modulo", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Ejecucion> ejecuciones = new ArrayList<>();

    // Getters y Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlaybook() {
        return playbook;
    }

    public void setPlaybook(String playbook) {
        this.playbook = playbook;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastJobRun() {
        return lastJobRun;
    }

    public void setLastJobRun(String lastJobRun) {
        this.lastJobRun = lastJobRun;
    }

    public boolean isLastJobFailed() {
        return lastJobFailed;
    }

    public void setLastJobFailed(boolean lastJobFailed) {
        this.lastJobFailed = lastJobFailed;
    }

    public String getJobTemplateId() {
        return jobTemplateId;
    }

    public void setJobTemplateId(String jobTemplateId) {
        this.jobTemplateId = jobTemplateId;
    }

    public List<Inventario> getInventarios() {
        return inventarios;
    }

    public void setInventarios(List<Inventario> inventarios) {
        this.inventarios = inventarios;
    }

    public List<Ejecucion> getEjecuciones() {
        return ejecuciones;
    }

    public void setEjecuciones(List<Ejecucion> ejecuciones) {
        this.ejecuciones = ejecuciones;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }
}