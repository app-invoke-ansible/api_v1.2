package entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;



@Entity
@Table(name = "ambitos")
public class Ambito {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @JsonIgnore  // Evitamos la serialización de la lista de categorías
    @OneToMany(mappedBy = "ambito", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Categoria> categoriaList;

    // Constructor vacío
    public Ambito() {
    }

    // Constructor con parámetros
    public Ambito(Long id, String name, List<Categoria> categoriaList) {
        this.id = id;
        this.name = name;
        this.categoriaList = categoriaList;
    }

    // Getters y Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Categoria> getCategoriaList() {
        return categoriaList;
    }

    public void setCategoriaList(List<Categoria> categoriaList) {
        this.categoriaList = categoriaList;
    }
}
