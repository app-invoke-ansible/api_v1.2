package services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import entities.*;
import exceptions.AnsibleServiceException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import repositories.*;
import websocket.LogWebSocket;

import java.io.IOException;
import java.time.LocalDateTime;

@ApplicationScoped
public class ModuloService {

    // Definición de la constante
    private static final String RESULTS_KEY = "results";

    private final ModuloRepository moduloRepository;
    private final AnsibleService ansibleService;
    private final EjecucionRepository ejecucionRepository;
    private final DetalleEjecucionRepository detalleEjecucionRepository;
    private final ObjectMapper objectMapper;
    private final InventarioRepository inventarioRepository;
    private final AmbitoRepository ambitoRepository;
    private final CategoriaRepository categoriaRepository;

    @Inject
    public ModuloService(ModuloRepository moduloRepository, AnsibleService ansibleService,
                         EjecucionRepository ejecucionRepository, DetalleEjecucionRepository detalleEjecucionRepository,
                         ObjectMapper objectMapper, InventarioRepository inventarioRepository, AmbitoRepository ambitoRepository, CategoriaRepository categoriaRepository) {
        this.moduloRepository = moduloRepository;
        this.ansibleService = ansibleService;
        this.ejecucionRepository = ejecucionRepository;
        this.detalleEjecucionRepository = detalleEjecucionRepository;
        this.objectMapper = objectMapper;
        this.inventarioRepository = inventarioRepository;
        this.ambitoRepository = ambitoRepository;
        this.categoriaRepository = categoriaRepository;
    }

    @Transactional
    public void automateModuloSaving(String jobTemplateId) throws IOException, AnsibleServiceException {
        String jsonResponse = ansibleService.getJobTemplateDetails(jobTemplateId);
        saveModuloFromJson(jsonResponse);
    }

    @Transactional
    public void saveModuloFromJson(String jsonResponse) throws IOException {
        JsonNode rootNode = objectMapper.readTree(jsonResponse);

        Modulo modulo = new Modulo();
        modulo.setName(rootNode.path("name").asText());

        // Obtener la descripción y establecer un valor predeterminado si está vacía
        String description = rootNode.path("description").asText();
        if (description == null || description.isEmpty()) {
            description = "Sin descripción"; // Valor predeterminado
        }
        modulo.setDescription(description); // Establecer la descripción

        modulo.setPlaybook(rootNode.path("playbook").asText());
        modulo.setJobType(rootNode.path("job_type").asText());
        modulo.setStatus(rootNode.path("status").asText());
        modulo.setLastJobRun(rootNode.path("last_job_run").asText());
        modulo.setLastJobFailed(rootNode.path("last_job_failed").asBoolean());
        modulo.setJobTemplateId(rootNode.path("id").asText());

        // Extraer la categoría y el ámbito del JSON
        JsonNode labelsNode = rootNode.path("summary_fields").path("labels").path("results");

        // Inicializar variables para categoría y ámbito
        String categoria = null;
        String ambito = null;

        // Iterar sobre las etiquetas para encontrar la categoría y el ámbito
        if (labelsNode.isArray()) {
            for (JsonNode label : labelsNode) {
                String labelName = label.path("name").asText();
                // Verificar si el nombre de la etiqueta contiene el prefijo y extraer el valor
                if (labelName.startsWith("[categoria]")) {
                    categoria = labelName.replace("[categoria]", "").trim(); // Eliminar el prefijo y espacios
                } else if (labelName.startsWith("[ambito]")) {
                    ambito = labelName.replace("[ambito]", "").trim(); // Eliminar el prefijo y espacios
                }
            }
        }

        //Buscar o crear el ambito en la base de datos
        Ambito ambitoObj = findOrCreateAmbito(ambito);

        //Buscar o crear la categoria en la base de datos
        Categoria categoriaObj = findOrCreateCategoria(categoria, ambitoObj);
        modulo.setCategoria(categoriaObj);

        // Establecer valores en el módulo
//        modulo.setCategoria(categoria != null ? categoria : "Sin categoría"); // Valor por defecto si no hay categoría
//        modulo.setAmbito(ambito != null ? ambito : "Sin ámbito"); // Valor por defecto si no hay ámbito

        // Persistir el módulo
        moduloRepository.persist(modulo);
        createOrUpdateInventory(rootNode, modulo);
    }

    private Ambito findOrCreateAmbito(String ambitoName) {
        Ambito ambito = ambitoRepository.findByName(ambitoName);
        if (ambito == null) {
            ambito = new Ambito();
            ambito.setName(ambitoName);
            ambitoRepository.persist(ambito);
        }
        return ambito;
    }

    private Categoria findOrCreateCategoria(String categoriaName, Ambito ambito) {
        Categoria categoria = categoriaRepository.findByNameAndAmbito(categoriaName, ambito);
        if (categoria == null) {
            categoria = new Categoria();
            categoria.setName(categoriaName);
            categoria.setAmbito(ambito);
            categoriaRepository.persist(categoria);
        }
        return categoria;
    }

    @Transactional
    public void createOrUpdateInventory(JsonNode rootNode, Modulo modulo) {
        String nombreInventario = rootNode.path("summary_fields").path("inventory").path("name").asText();
        String descripcionInventario = rootNode.path("summary_fields").path("inventory").path("description").asText();

        if (descripcionInventario == null || descripcionInventario.isEmpty()) {
            descripcionInventario = "Sin descripción";
        }

        Inventario inventario = inventarioRepository.findByNombre(nombreInventario);
        if (inventario == null) {
            inventario = new Inventario();
            inventario.setNombreInventario(nombreInventario);
            inventario.setDescription(descripcionInventario);
            inventarioRepository.persist(inventario);
        } else {
            inventario.setDescription(descripcionInventario);
            inventarioRepository.getEntityManager().merge(inventario);
        }

        if (!modulo.getInventarios().contains(inventario)) {
            modulo.getInventarios().add(inventario);
        }
        if (!inventario.getModulos().contains(modulo)) {
            inventario.getModulos().add(modulo);
        }
    }

    @Transactional
    public void executeModulo(Long moduloId) throws AnsibleServiceException, IOException {
        LogWebSocket.sendLog("Iniciando ejecución del módulo con ID: " + moduloId);

        Modulo modulo = moduloRepository.findById(moduloId);
        if (modulo == null) {
            LogWebSocket.sendLog("Módulo no encontrado con ID: " + moduloId);
            throw new AnsibleServiceException("Módulo no encontrado con ID: " + moduloId);
        }

        Ejecucion ejecucion = new Ejecucion();
        ejecucion.setModulo(modulo);
        ejecucion.setStartTime(LocalDateTime.now());
        ejecucion.setStatus("pending");

        try {
            String jobTemplateId = modulo.getJobTemplateId();
            LogWebSocket.sendLog("Ejecutando job template con ID: " + jobTemplateId);

            String response = ansibleService.executeJobTemplate(jobTemplateId);
            JsonNode jsonResponse = objectMapper.readTree(response);
            String jobId = jsonResponse.path("job").asText();

            ejecucion.setJobId(jobId);
            LogWebSocket.sendLog("Job ID almacenado: " + jobId);

            monitorJobExecution(ejecucion, jobId);

            // Establecer el tiempo de finalización
            ejecucion.setEndTime(LocalDateTime.now());
            ejecucion.setStatus(determineFinalStatus(ejecucion.getStatus()));
            LogWebSocket.sendLog("Ejecución del módulo con ID: " + moduloId + " finalizada con estado: " + ejecucion.getStatus());

            // Calcular la duración y actualizar el módulo
            if (ejecucion.getStartTime() != null && ejecucion.getEndTime() != null) {
                long durationInSeconds = java.time.Duration.between(ejecucion.getStartTime(), ejecucion.getEndTime()).toSeconds();
                String duration = durationInSeconds / 3600 + "hrs " + (durationInSeconds % 3600) / 60 + "min";
                modulo.setDuracion(duration); // Actualizar la duración en el módulo
            }

            ejecucionRepository.persist(ejecucion);

            Long groupId = 1L;
            String hostsResponse = ansibleService.getHostsFromGroup(groupId);
            saveExecutionDetails(ejecucion, hostsResponse);
        } catch (Exception e) {
            LogWebSocket.sendLog("Error durante la ejecución del módulo con ID: " + moduloId + ": " + e.getMessage());
            throw new AnsibleServiceException("Error durante la ejecución del módulo: " + e.getMessage(), e);
        }
    }

    private void monitorJobExecution(Ejecucion ejecucion, String jobId) throws AnsibleServiceException {
        String status;
        do {
            status = ansibleService.getJobStatus(jobId);
            LogWebSocket.sendLog("Estado del trabajo: " + status);

            String jobLogs = ansibleService.getJobOutput(jobId);
            if (jobLogs != null && !jobLogs.isEmpty()) {
                LogWebSocket.sendLog("Logs del trabajo:\n" + jobLogs);
            }

            ejecucion.setStatus(status);
            sleepForInterval();
        } while (status.equals("pending") || status.equals("running"));
    }

    private void sleepForInterval() throws AnsibleServiceException {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new AnsibleServiceException(
                    "El hilo fue interrumpido mientras esperaba el estado del trabajo.", e);
        }
    }

    private String determineFinalStatus(String status) {
        return switch (status) {
            case "failed" -> "failed";
            case "canceled" -> "canceled";
            default -> "success";
        };
    }

    @Transactional
    public void saveExecutionDetails(Ejecucion ejecucion, String hostsResponse) throws IOException {
        JsonNode hostsNode = objectMapper.readTree(hostsResponse);

        if (hostsNode.has(RESULTS_KEY) && hostsNode.path(RESULTS_KEY).isArray()) {
            for (JsonNode hostNode : hostsNode.path(RESULTS_KEY)) {
                DetalleEjecucion detalleEjecucion = new DetalleEjecucion();
                detalleEjecucion.setEjecucion(ejecucion);
                detalleEjecucion.setOutput("Output de la ejecución");
                detalleEjecucion.setErrorDetails(null);

                String hostName = hostNode.path("name").asText();
                String variables = hostNode.path("variables").asText();

                String ipAddress = extractIpFromVariables(variables);

                if (hostName == null || hostName.isEmpty()) {
                    throw new IllegalArgumentException("El nombre de la máquina no debe ser nulo o vacío");
                }
                if (ipAddress.isEmpty()) {
                    throw new IllegalArgumentException("La dirección IP no debe ser nula o vacía");
                }

                detalleEjecucion.setNombreMaquina(hostName);
                detalleEjecucion.setHostMaquina(ipAddress);

                detalleEjecucionRepository.persist(detalleEjecucion);
            }
        } else {
            throw new IllegalArgumentException("No se encontraron hosts en la respuesta JSON");
        }
    }

    private String extractIpFromVariables(String variables) {
        String[] lines = variables.split("\n");
        for (String line : lines) {
            if (line.startsWith("ansible_host:")) {
                return line.split(":")[1].trim();
            }
        }
        return "IP no encontrada";
    }

    @Transactional
    public void stopModuloExecution(String jobId) throws AnsibleServiceException {
        LogWebSocket.sendLog("Deteniendo la ejecución del módulo con Job ID: " + jobId);

        try {
            ansibleService.cancelJob(jobId);
            LogWebSocket.sendLog("Ejecución con Job ID: " + jobId + " ha sido detenida.");
        } catch (Exception e) {
            LogWebSocket.sendLog("Error al detener la ejecución con Job ID: " + jobId + ": " + e.getMessage());
            throw new AnsibleServiceException("Error al detener la ejecución del módulo: " + e.getMessage(), e);
        }
    }

}