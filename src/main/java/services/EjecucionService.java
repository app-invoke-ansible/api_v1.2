package services;

import dto.EjecucionDTO;
import dto.ModuloEjecucionesDTO;
import entities.Ejecucion;
import entities.Modulo;
import jakarta.transaction.Transactional;
import repositories.EjecucionRepository;
import repositories.ModuloRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import exceptions.AnsibleServiceException;

@ApplicationScoped
public class EjecucionService {

    private final ModuloRepository moduloRepository;
    private final EjecucionRepository ejecucionRepository;
    private final AnsibleService ansibleService;

    @Inject
    public EjecucionService(ModuloRepository moduloRepository, EjecucionRepository ejecucionRepository, AnsibleService ansibleService) {
        this.moduloRepository = moduloRepository;
        this.ejecucionRepository = ejecucionRepository;
        this.ansibleService = ansibleService;
    }

    @Transactional
    public ModuloEjecucionesDTO getEjecucionesPorModuloId(Long moduloId) {
        Modulo modulo = moduloRepository.findById(moduloId);
        if (modulo == null) {
            throw new IllegalArgumentException("Módulo no encontrado con ID: " + moduloId);
        }

        ModuloEjecucionesDTO dto = new ModuloEjecucionesDTO();
        dto.setModuloId(modulo.getId());
        dto.setModuloName(modulo.getName());

        List<ModuloEjecucionesDTO.EjecucionDTO> ejecuciones = modulo.getEjecuciones().stream()
                .map(ejecucion -> {
                    ModuloEjecucionesDTO.EjecucionDTO ejecucionDTO = new ModuloEjecucionesDTO.EjecucionDTO();
                    ejecucionDTO.setId(ejecucion.getId());
                    ejecucionDTO.setStatus(ejecucion.getStatus());
                    ejecucionDTO.setStartTime(ejecucion.getStartTime());
                    ejecucionDTO.setEndTime(ejecucion.getEndTime());

                    List<ModuloEjecucionesDTO.DetalleEjecucionDTO> detalles = ejecucion.getDetallesEjecucion().stream()
                            .map(detalle -> {
                                ModuloEjecucionesDTO.DetalleEjecucionDTO detalleDTO = new ModuloEjecucionesDTO.DetalleEjecucionDTO();
                                detalleDTO.setNombreMaquina(detalle.getNombreMaquina());
                                detalleDTO.setHostMaquina(detalle.getHostMaquina());
                                detalleDTO.setOutput(detalle.getOutput());
                                detalleDTO.setErrorDetails(detalle.getErrorDetails());
                                return detalleDTO;
                            })
                            .toList();

                    ejecucionDTO.setDetalles(detalles);
                    return ejecucionDTO;
                })
                .toList();

        dto.setEjecuciones(ejecuciones);
        return dto;
    }

    @Transactional
    public List<EjecucionDTO> obtenerUltimasEjecuciones() {
        List<Ejecucion> ejecuciones = ejecucionRepository.findTop10ByOrderByStartTimeDesc();
        return ejecuciones.stream().map(ejecucion -> {
            LocalDateTime startTime = ejecucion.getStartTime();
            LocalDateTime endTime = ejecucion.getEndTime();

            Duration duration = (endTime != null) ? Duration.between(startTime, endTime) : Duration.ZERO;
            String formattedDuration = formatDuration(duration);

            String ambito = ejecucion.getModulo().getCategoria().getAmbito().getName();
            String categoria = ejecucion.getModulo().getCategoria().getName();
            String jobId = ejecucion.getJobId();  // Obtener el jobId de la entidad Ejecucion

            return new EjecucionDTO(
                    startTime,
                    formattedDuration,
                    ambito,
                    categoria,
                    ejecucion.getStatus(),
                    ejecucion.getModulo().getName(),
                    ejecucion.getModulo().getId(),
                    jobId  // Pasar el jobId al constructor de EjecucionDTO
            );
        }).toList();
    }

    @Transactional
    public String obtenerLogDeEjecucion(String jobId) {
        try {
            return ansibleService.getJobOutput(jobId);
        } catch (AnsibleServiceException e) {
            throw new RuntimeException("Error al obtener el log de ejecución con ID: " + jobId, e);
        }
    }

    private String formatDuration(Duration duration) {
        long totalSeconds = duration.getSeconds();
        long hours = totalSeconds / 3600;
        long minutes = (totalSeconds % 3600) / 60;
        long seconds = totalSeconds % 60;
        return String.format("%d h, %d min, %d sec", hours, minutes, seconds);
    }
}
