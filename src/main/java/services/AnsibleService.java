package services;

import exceptions.AnsibleServiceException;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import restclient.AnsibleClient;
import websocket.LogWebSocket;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@ApplicationScoped
public class AnsibleService {

    private static final Logger logger = LoggerFactory.getLogger(AnsibleService.class);
    private static final String ERROR_DETAILS = ". Detalles: ";
    private static final String ERROR_EXECUTING_JOB_TEMPLATE = "Error al ejecutar el job template con el ID: ";
    private static final String ERROR_OBTAINING_JOB_TEMPLATE_DETAILS = "Error al obtener los detalles del job template con el ID: ";
    private static final String ERROR_OBTAINING_JOB_OUTPUT = "Error al obtener la salida del trabajo con el ID: ";
    private static final String ERROR_OBTAINING_JOB_STATUS = "Error al obtener el estado del trabajo con ID: ";
    private static final String ERROR_OBTAINING_HOSTS = "Error al obtener los hosts del grupo con ID: ";
    private static final String ERROR_CANCELING_JOB = "Error al cancelar el trabajo en Ansible: ";

    private final ObjectMapper objectMapper;
    private final AnsibleClient ansibleClient;

    @Inject
    public AnsibleService(@RestClient AnsibleClient ansibleClient) {
        this.objectMapper = new ObjectMapper();
        this.ansibleClient = ansibleClient;
    }

    public String executeJobTemplate(String jobTemplateId) throws AnsibleServiceException {
        try {
            String response = ansibleClient.launchJobTemplate(jobTemplateId);
            logger.info("Respuesta de la ejecución del job template: {}", response);
            return response;
        } catch (Exception e) {
            throw new AnsibleServiceException(
                    ERROR_EXECUTING_JOB_TEMPLATE + jobTemplateId + ERROR_DETAILS + e.getMessage(), e);
        }
    }

    public String getJobTemplateDetails(String jobTemplateId) throws AnsibleServiceException {
        try {
            String response = ansibleClient.getJobTemplateDetails(jobTemplateId);
            logger.info("Detalles del job template: {}", response);
            return response;
        } catch (Exception e) {
            throw new AnsibleServiceException(
                    ERROR_OBTAINING_JOB_TEMPLATE_DETAILS + jobTemplateId + ERROR_DETAILS + e.getMessage(), e);
        }
    }

    public String getJobOutput(String jobId) throws AnsibleServiceException {
        try {
            String output = ansibleClient.getJobOutput(jobId);
            logger.info("Salida del trabajo obtenida: {}", output);
            return output;
        } catch (Exception e) {
            throw new AnsibleServiceException(
                    ERROR_OBTAINING_JOB_OUTPUT + jobId + ERROR_DETAILS + e.getMessage(), e);
        }
    }

    public String getJobStatus(String jobId) throws AnsibleServiceException {
        try {
            String response = ansibleClient.getJobDetails(jobId);
            JsonNode jsonResponse = objectMapper.readTree(response);
            String status = jsonResponse.path("status").asText();
            logger.info("Estado del trabajo con ID {}: {}", jobId, status);
            return status;
        } catch (Exception e) {
            throw new AnsibleServiceException(
                    ERROR_OBTAINING_JOB_STATUS + jobId + ERROR_DETAILS + e.getMessage(), e);
        }
    }

    public String getHostsFromGroup(Long groupId) throws AnsibleServiceException {
        try {
            String response = ansibleClient.getHostsFromGroup(groupId);
            logger.info("Hosts en el grupo con ID {}: {}", groupId, response);
            return response;
        } catch (Exception e) {
            throw new AnsibleServiceException(
                    ERROR_OBTAINING_HOSTS + groupId + ERROR_DETAILS + e.getMessage(), e);
        }
    }

    public void cancelJob(String jobId) throws AnsibleServiceException {
        try {
            LogWebSocket.sendLog("Cancelando trabajo con ID: " + jobId);
            String response = ansibleClient.cancelJob(jobId);
            logger.info("Trabajo cancelado exitosamente: {}. Respuesta: {}", jobId, response);
        } catch (Exception e) {
            throw new AnsibleServiceException(
                    ERROR_CANCELING_JOB + jobId + ERROR_DETAILS + e.getMessage(), e);
        }
    }

    public Long getGroupIdByName(String groupName) throws AnsibleServiceException {
        try {
            String response = ansibleClient.getGroups();
            JsonNode jsonResponse = objectMapper.readTree(response);

            for (JsonNode groupNode : jsonResponse.path("results")) {
                String name = groupNode.path("name").asText();
                if (name.equalsIgnoreCase(groupName)) {
                    return groupNode.path("id").asLong(); // Retorna el ID del grupo
                }
            }

            throw new AnsibleServiceException("Grupo no encontrado: " + groupName);
        } catch (Exception e) {
            throw new AnsibleServiceException("Error al obtener el ID del grupo: " + groupName, e);
        }
    }
}
