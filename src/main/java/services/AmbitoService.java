package services;

import entities.Ambito;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import repositories.AmbitoRepository;

import java.util.List;

@ApplicationScoped
public class AmbitoService {

    private final AmbitoRepository ambitoRepository;

    @Inject
    public AmbitoService(AmbitoRepository ambitoRepository) {
        this.ambitoRepository = ambitoRepository;
    }

    @Transactional
    public List<Ambito> getAllAmbitos() {
        return ambitoRepository.listAll();  // Esto retornará todos los Ambitos de la BD
    }
}
