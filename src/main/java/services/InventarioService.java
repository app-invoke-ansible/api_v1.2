package services;

import entities.Inventario;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;
import repositories.InventarioRepository;

@ApplicationScoped
public class InventarioService {

    private final InventarioRepository inventarioRepository;

    public InventarioService(InventarioRepository inventarioRepository) {
        this.inventarioRepository = inventarioRepository;
    }

    public Inventario findById(Long id) {
        return inventarioRepository.findById(id);
    }

    @Transactional
    public void save(Inventario inventario) {
        inventarioRepository.getEntityManager().merge(inventario); // Usar merge para manejar entidades existentes
    }
}