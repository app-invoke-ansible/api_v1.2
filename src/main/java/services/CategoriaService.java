package services;

import entities.Categoria;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.hibernate.Hibernate;
import repositories.CategoriaRepository;

import java.util.List;
import java.util.Optional;

@ApplicationScoped
public class CategoriaService {

    private final CategoriaRepository categoriaRepository;

    @Inject
    public CategoriaService(CategoriaRepository categoriaRepository) {
        this.categoriaRepository = categoriaRepository;
    }

    @Transactional
    public List<Categoria> getAllCategorias() {
        List<Categoria> categorias = categoriaRepository.listAll();
        categorias.forEach(categoria -> {
            Hibernate.initialize(categoria.getAmbito());
        });
        return categorias;
    }

    @Transactional
    public Optional<List<Categoria>> getCategoriasByCategoriaId(Long id) {
        // 1. Obtener la categoría por su ID
        Categoria categoria = categoriaRepository.findByIdWithRelations(id);
        if (categoria == null) {
            return Optional.empty();
        }

        // 2. Usar el Ambito de la categoría obtenida para obtener todas las categorías relacionadas
        Long ambitoId = categoria.getAmbito().getId();
        List<Categoria> categoriasRelacionadas = categoriaRepository.findByAmbitoId(ambitoId);

        return Optional.of(categoriasRelacionadas);
    }

    @Transactional
    public List<Categoria> getCategoriasByAmbitoId(Long ambitoId) {
        return categoriaRepository.findByAmbitoId(ambitoId);
    }

    @Transactional
    public Categoria createCategoria(Categoria categoria) {
        categoriaRepository.persist(categoria);
        return categoria;
    }

    @Transactional
    public Categoria updateCategoria(Long id, Categoria categoriaDetails) {
        Categoria categoria = categoriaRepository.findById(id);
        if (categoria == null) {
            throw new IllegalArgumentException("Categoría no encontrada con ID: " + id);
        }

        categoria.setName(categoriaDetails.getName());

        if (categoriaDetails.getAmbito() != null) {
            categoria.setAmbito(categoriaDetails.getAmbito());
        }

        categoriaRepository.persist(categoria);
        return categoria;
    }

    @Transactional
    public void deleteCategoria(Long id) {
        Categoria categoria = categoriaRepository.findById(id);
        if (categoria != null) {
            categoriaRepository.delete(categoria);
        }
    }
}
