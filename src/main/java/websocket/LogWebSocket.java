package websocket;

import jakarta.websocket.OnClose;
import jakarta.websocket.OnMessage;
import jakarta.websocket.OnOpen;
import jakarta.websocket.Session;
import jakarta.websocket.server.ServerEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.CopyOnWriteArraySet;

@ServerEndpoint("/logs")
public class LogWebSocket {

    private static final Logger logger = LoggerFactory.getLogger(LogWebSocket.class);
    private static final CopyOnWriteArraySet<Session> sessions = new CopyOnWriteArraySet<>();

    @OnOpen
    public void onOpen(Session session) {
        sessions.add(session);
        logger.info("Nueva conexión WebSocket: {}", session.getId());
    }

    @OnClose
    public void onClose(Session session) {
        sessions.remove(session);
        logger.info("Conexión WebSocket cerrada: {}", session.getId());
    }

    @OnMessage
    public void onMessage(String message, Session session) {
        logger.info("Mensaje recibido de {}: {}", session.getId(), message);
    }

    public static void sendLog(String log) {
        for (Session session : sessions) {
            try {
                session.getBasicRemote().sendText(log);
            } catch (IOException e) {
                logger.error("Error al enviar log a la sesión {}: {}", session.getId(), e.getMessage());
            }
        }
    }
}