package dto;

import java.time.LocalDateTime;

public class EjecucionDTO {

    private LocalDateTime fechaHoraLanzamiento;
    private String duracion;
    private String ambito;
    private String categoria;
    private String estado;
    private String modulo;
    private Long moduloId;
    private String jobId;  // Nuevo campo para el jobId

    // Constructor actualizado con jobId
    public EjecucionDTO(LocalDateTime fechaHoraLanzamiento, String duracion, String ambito, String categoria, String estado, String modulo, Long moduloId, String jobId) {
        this.fechaHoraLanzamiento = fechaHoraLanzamiento;
        this.duracion = duracion;
        this.ambito = ambito;
        this.categoria = categoria;
        this.estado = estado;
        this.modulo = modulo;
        this.moduloId = moduloId;
        this.jobId = jobId;  // Inicializar el jobId
    }

    // Getters y Setters
    public LocalDateTime getFechaHoraLanzamiento() {
        return fechaHoraLanzamiento;
    }

    public void setFechaHoraLanzamiento(LocalDateTime fechaHoraLanzamiento) {
        this.fechaHoraLanzamiento = fechaHoraLanzamiento;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public Long getModuloId() {
        return moduloId;
    }

    public void setModuloId(Long moduloId) {
        this.moduloId = moduloId;
    }

    public String getJobId() {  // Nuevo getter para jobId
        return jobId;
    }

    public void setJobId(String jobId) {  // Nuevo setter para jobId
        this.jobId = jobId;
    }
}
