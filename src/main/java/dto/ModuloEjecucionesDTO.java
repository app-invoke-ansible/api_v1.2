package dto;

import java.time.LocalDateTime;
import java.util.List;

public class ModuloEjecucionesDTO {
    private Long moduloId;
    private String moduloName;
    private List<EjecucionDTO> ejecuciones;

    // Getters y Setters
    public Long getModuloId() {
        return moduloId;
    }

    public void setModuloId(Long moduloId) {
        this.moduloId = moduloId;
    }

    public String getModuloName() {
        return moduloName;
    }

    public void setModuloName(String moduloName) {
        this.moduloName = moduloName;
    }

    public List<EjecucionDTO> getEjecuciones() {
        return ejecuciones;
    }

    public void setEjecuciones(List<EjecucionDTO> ejecuciones) {
        this.ejecuciones = ejecuciones;
    }

    // Clase anidada EjecucionDTO
    public static class EjecucionDTO {
        private Long id;
        private String status;
        private LocalDateTime startTime;
        private LocalDateTime endTime;
        private List<DetalleEjecucionDTO> detalles;

        // Getters y Setters
        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public LocalDateTime getStartTime() {
            return startTime;
        }

        public void setStartTime(LocalDateTime startTime) {
            this.startTime = startTime;
        }

        public LocalDateTime getEndTime() {
            return endTime;
        }

        public void setEndTime(LocalDateTime endTime) {
            this.endTime = endTime;
        }

        public List<DetalleEjecucionDTO> getDetalles() {
            return detalles;
        }

        public void setDetalles(List<DetalleEjecucionDTO> detalles) {
            this.detalles = detalles;
        }
    }

    // Clase anidada DetalleEjecucionDTO
    public static class DetalleEjecucionDTO {
        private String nombreMaquina;
        private String hostMaquina;
        private String output;
        private String errorDetails;

        // Getters y Setters
        public String getNombreMaquina() {
            return nombreMaquina;
        }

        public void setNombreMaquina(String nombreMaquina) {
            this.nombreMaquina = nombreMaquina;
        }

        public String getHostMaquina() {
            return hostMaquina;
        }

        public void setHostMaquina(String hostMaquina) {
            this.hostMaquina = hostMaquina;
        }

        public String getOutput() {
            return output;
        }

        public void setOutput(String output) {
            this.output = output;
        }

        public String getErrorDetails() {
            return errorDetails;
        }

        public void setErrorDetails(String errorDetails) {
            this.errorDetails = errorDetails;
        }
    }
}