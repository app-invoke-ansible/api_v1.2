package exceptions;

public class AnsibleServiceException extends Exception {

    public AnsibleServiceException(String message) {
        super(message);
    }

    public AnsibleServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
