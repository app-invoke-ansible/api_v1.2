package restclient;

import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@RegisterRestClient
public interface AnsibleClient {

    @POST
    @Path("/api/v2/job_templates/{id}/launch/")
    @Produces(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Authorization", value = "Basic YWRtaW46SW52b2tlLjIyMTIy")
    String launchJobTemplate(@PathParam("id") String jobTemplateId);

    @GET
    @Path("/api/v2/job_templates/{id}/")
    @Produces(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Authorization", value = "Basic YWRtaW46SW52b2tlLjIyMTIy")
    String getJobTemplateDetails(@PathParam("id") String jobTemplateId);

    @GET
    @Path("/api/v2/jobs/{id}/stdout/")
    @Produces(MediaType.TEXT_PLAIN)
    @ClientHeaderParam(name = "Authorization", value = "Basic YWRtaW46SW52b2tlLjIyMTIy")
    String getJobOutput(@PathParam("id") String jobId);

    @GET
    @Path("/api/v2/jobs/{id}/")
    @Produces(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Authorization", value = "Basic YWRtaW46SW52b2tlLjIyMTIy")
    String getJobDetails(@PathParam("id") String jobId);

    @GET
    @Path("/api/v2/groups/{id}/hosts/")
    @Produces(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Authorization", value = "Basic YWRtaW46SW52b2tlLjIyMTIy")
    String getHostsFromGroup(@PathParam("id") Long groupId);

    @POST
    @Path("/api/v2/jobs/{id}/cancel/")
    @Produces(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Authorization", value = "Basic YWRtaW46SW52b2tlLjIyMTIy")
    String cancelJob(@PathParam("id") String jobId);

    @GET
    @Path("/api/v2/groups/")
    @Produces(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = "Authorization", value = "Basic YWRtaW46SW52b2tlLjIyMTIy")
    String getGroups();
}
