package services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.fasterxml.jackson.databind.JsonNode;
import entities.DetalleEjecucion;
import entities.Ejecucion;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import entities.Modulo;
import entities.Inventario;
import exceptions.AnsibleServiceException;
import repositories.DetalleEjecucionRepository;
import repositories.EjecucionRepository;
import repositories.ModuloRepository;
import repositories.InventarioRepository;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;

class ModuloServiceTest {
    @Mock
    private ModuloRepository moduloRepository;

    @Mock
    private AnsibleService ansibleService;

    @Mock
    private EjecucionRepository ejecucionRepository;

    @Mock
    private DetalleEjecucionRepository detalleEjecucionRepository;

    @Mock
    private InventarioRepository inventarioRepository;

    @InjectMocks
    private ModuloService moduloService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    private Modulo createTestModulo() {
        Modulo modulo = new Modulo();
        modulo.setId(1L);
        modulo.setName("Test Module");
        modulo.setDescription("A test module description");
        modulo.setPlaybook("test_playbook.yml");
        modulo.setJobType("test");
        modulo.setStatus("active");
        modulo.setLastJobRun("2024-10-04");
        modulo.setLastJobFailed(false);
        modulo.setJobTemplateId("42");
        modulo.setEjecuciones(new ArrayList<>()); // Inicializa la lista de ejecuciones
        modulo.setInventarios(new ArrayList<>()); // Inicializa la lista de inventarios
        return modulo;
    }

    private Ejecucion createTestEjecucion(Modulo modulo) {
        Ejecucion ejecucion = new Ejecucion();
        ejecucion.setId(1L);
        ejecucion.setModulo(modulo);
        ejecucion.setJobId("42");
        ejecucion.setStartTime(LocalDateTime.now());
        ejecucion.setEndTime(LocalDateTime.now().plusHours(1)); // Simulando un tiempo de finalización
        ejecucion.setStatus("success");
        ejecucion.setDetallesEjecucion(new ArrayList<>()); // Inicializa la lista de detalles de ejecución
        return ejecucion;
    }

    @Test
    void testAutomateModuloSaving() throws AnsibleServiceException, IOException {
        // Arrange
        String jobTemplateId = "42";
        String jsonResponse = "{\"name\":\"Test Module\",\"description\":\"A test module\",\"playbook\":\"test_playbook.yml\",\"job_type\":\"test\",\"status\":\"active\",\"last_job_run\":\"2024-10-04\",\"last_job_failed\":false,\"id\":\"42\"}";

        // Mock the behavior of ansibleService
        when(ansibleService.getJobTemplateDetails(jobTemplateId)).thenReturn(jsonResponse);

        // Act
        moduloService.automateModuloSaving(jobTemplateId);

        // Assert
        verify(ansibleService).getJobTemplateDetails(jobTemplateId);
        verify(moduloRepository).persist(isA(Modulo.class)); // Verifica que se persista un objeto Módulo
    }

    @Test
    void testSaveModuloFromJson_Success() throws IOException {
        // Arrange
        String jsonResponse = "{\"name\":\"Test Module\",\"description\":\"A test module\",\"playbook\":\"test_playbook.yml\",\"job_type\":\"test\",\"status\":\"active\",\"last_job_run\":\"2024-10-04\",\"last_job_failed\":false,\"id\":\"42\"}";

        // Act
        moduloService.saveModuloFromJson(jsonResponse);

        // Assert
        verify(moduloRepository).persist(isA(Modulo.class)); // Verifica que se persista un objeto Módulo
    }

    @Test
    void testSaveModuloFromJson_Failure() {
        // Arrange
        String invalidJsonResponse = "{invalid json}"; // Simulando un JSON inválido

        // Act and Assert
        assertThrows(IOException.class, () -> moduloService.saveModuloFromJson(invalidJsonResponse));
    }

    @Test
    void testCreateOrUpdateInventory_Success_NewInventario() {
        // Arrange
        JsonNode rootNode = mock(JsonNode.class);
        JsonNode summaryFieldsNode = mock(JsonNode.class);
        JsonNode inventoryNode = mock(JsonNode.class);

        when(rootNode.path("summary_fields")).thenReturn(summaryFieldsNode);
        when(summaryFieldsNode.path("inventory")).thenReturn(inventoryNode);
        when(inventoryNode.path("name").asText()).thenReturn("New Inventory");
        when(inventoryNode.path("description").asText()).thenReturn("New Inventory Description");

        Modulo modulo = new Modulo();
        modulo.setInventarios(new ArrayList<>());

        // Mock the behavior of inventarioRepository
        when(inventarioRepository.findByNombre("New Inventory")).thenReturn(null); // Simulando que no existe

        // Act
        moduloService.createOrUpdateInventory(rootNode, modulo);

        // Assert
        verify(inventarioRepository).persist(isA(Inventario.class)); // Verifica que se persista un nuevo Inventario
        assertEquals(1, modulo.getInventarios().size()); // Verifica que el módulo tenga un inventario
    }

    @Test
    void testCreateOrUpdateInventory_Success_ExistingInventario() {
        // Arrange
        JsonNode rootNode = mock(JsonNode.class);
        JsonNode summaryFieldsNode = mock(JsonNode.class);
        JsonNode inventoryNode = mock(JsonNode.class);

        when(rootNode.path("summary_fields")).thenReturn(summaryFieldsNode);
        when(summaryFieldsNode.path("inventory")).thenReturn(inventoryNode);
        when(inventoryNode.path("name").asText()).thenReturn("Existing Inventory");
        when(inventoryNode.path("description").asText()).thenReturn("Updated Description");

        Modulo modulo = new Modulo();
        modulo.setInventarios(new ArrayList<>());

        Inventario existingInventario = new Inventario();
        existingInventario.setNombreInventario("Existing Inventory");
        existingInventario.setDescription("Old Description");

        // Mock the behavior of inventarioRepository
        when(inventarioRepository.findByNombre("Existing Inventory")).thenReturn(existingInventario); // Simulando que existe

        // Act
        moduloService.createOrUpdateInventory(rootNode, modulo);

        // Assert
        verify(inventarioRepository).getEntityManager(); // Verifica que se obtenga el EntityManager
        verify(inventarioRepository).persist(existingInventario); // Verifica que no se persista un nuevo Inventario
        assertEquals(1, modulo.getInventarios().size()); // Verifica que el módulo tenga un inventario
        assertEquals("Updated Description", existingInventario.getDescription()); // Verifica que la descripción se haya actualizado
    }

    @Test
    void testExecuteModulo_Success() throws AnsibleServiceException, IOException {
        // Arrange
        Long moduloId = 1L;
        Modulo modulo = createTestModulo(); // Usa el método para crear un objeto Modulo
        modulo.setJobTemplateId("42"); // Asegúrate de que tenga un ID de job template

        when(moduloRepository.findById(moduloId)).thenReturn(modulo);
        when(ansibleService.executeJobTemplate(modulo.getJobTemplateId())).thenReturn("{\"job\":\"12345\"}"); // Simulando respuesta
        when(ansibleService.getHostsFromGroup(anyLong())).thenReturn("Host1, Host2"); // Simulando respuesta de hosts

        // Act
        moduloService.executeModulo(moduloId);

        // Assert
        verify(moduloRepository).findById(moduloId); // Verifica que se busque el módulo
        verify(ansibleService).executeJobTemplate(modulo.getJobTemplateId()); // Verifica que se ejecute el job témplate
        verify(ejecucionRepository).persist(isA(Ejecucion.class)); // Verifica que se persista una ejecución
    }

    @Test
    void testExecuteModulo_Failure_ModuleNotFound() {
        // Arrange
        Long moduloId = 1L;
        when(moduloRepository.findById(moduloId)).thenReturn(null); // Simulando que no se encuentra el módulo

        // Act and Assert
        AnsibleServiceException exception = assertThrows(AnsibleServiceException.class, () -> moduloService.executeModulo(moduloId));

        assertTrue(exception.getMessage().contains("Módulo no encontrado con ID: " + moduloId));
        verify(moduloRepository).findById(moduloId); // Verifica que se busque el módulo
    }

    @Test
    void testExecuteModulo_Failure_JobExecutionError() throws Exception {
        // Arrange
        Long moduloId = 1L;
        Modulo modulo = createTestModulo();
        modulo.setJobTemplateId("42");

        when(moduloRepository.findById(moduloId)).thenReturn(modulo);
        when(ansibleService.executeJobTemplate(modulo.getJobTemplateId())).thenThrow(new RuntimeException("Error")); // Simulando error

        // Act and Assert
        AnsibleServiceException exception = assertThrows(AnsibleServiceException.class, () -> moduloService.executeModulo(moduloId));

        assertTrue(exception.getMessage().contains("Error durante la ejecución del módulo: Error"));
        verify(moduloRepository).findById(moduloId); // Verifica que se busque el módulo
    }

    @Test
    void testSaveExecutionDetails_Success() throws IOException {
        // Arrange
        String hostsResponse = "{\"results\":[{\"name\":\"Host1\",\"variables\":\"ansible_host: 192.168.1.1\"},{\"name\":\"Host2\",\"variables\":\"ansible_host: 192.168.1.2\"}]}";
        Ejecucion ejecucion = createTestEjecucion(createTestModulo()); // Usa el método para crear un objeto Ejecucion

        // Act
        moduloService.saveExecutionDetails(ejecucion, hostsResponse);

        // Assert
        verify(detalleEjecucionRepository, times(2)).persist(isA(DetalleEjecucion.class)); // Verifica que se persista un objeto DetalleEjecucion por cada host
    }

    @Test
    void testSaveExecutionDetails_Failure_EmptyHostName() {
        // Arrange
        String hostsResponse = "{\"results\":[{\"name\":\"\",\"variables\":\"ansible_host: 192.168.1.1\"}]}";
        Ejecucion ejecucion = createTestEjecucion(createTestModulo());

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> moduloService.saveExecutionDetails(ejecucion, hostsResponse));
    }

    @Test
    void testSaveExecutionDetails_Failure_EmptyIpAddress() {
        // Arrange
        String hostsResponse = "{\"results\":[{\"name\":\"Host1\",\"variables\":\"\"}]}";
        Ejecucion ejecucion = createTestEjecucion(createTestModulo());

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> moduloService.saveExecutionDetails(ejecucion, hostsResponse));
    }

    @Test
    void testSaveExecutionDetails_Failure_NoHosts() {
        // Arrange
        String hostsResponse = "{\"results\":[]}";
        Ejecucion ejecucion = createTestEjecucion(createTestModulo());

        // Act and Assert
        assertThrows(IllegalArgumentException.class, () -> moduloService.saveExecutionDetails(ejecucion, hostsResponse));
    }

    @Test
    void testStopModuloExecution_Success() throws AnsibleServiceException {
        // Arrange
        String jobId = "42";
        doNothing().when(ansibleService).cancelJob(jobId); // Simulando que la cancelación es exitosa

        // Act
        moduloService.stopModuloExecution(jobId);

        // Assert
        verify(ansibleService).cancelJob(jobId); // Verifica que el método fue llamado
    }

    @Test
    void testStopModuloExecution_Failure() {
        // Arrange
        String jobId = "42";
        try {
            doThrow(new RuntimeException("Error")).when(ansibleService).cancelJob(jobId); // Simulando un error en la cancelación
        } catch (AnsibleServiceException e) {
            throw new RuntimeException(e);
        }

        // Act and Assert
        AnsibleServiceException exception = assertThrows(AnsibleServiceException.class, () -> moduloService.stopModuloExecution(jobId));

        assertTrue(exception.getMessage().contains("Error al detener la ejecución del módulo: Error"));
        try {
            verify(ansibleService).cancelJob(jobId); // Verifica que el método fue llamado
        } catch (AnsibleServiceException e) {
            throw new RuntimeException(e);
        }
    }


}