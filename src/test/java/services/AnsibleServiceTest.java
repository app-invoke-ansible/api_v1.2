package services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import restclient.AnsibleClient;
import exceptions.AnsibleServiceException;

@ExtendWith(MockitoExtension.class)
class AnsibleServiceTest {
    @Mock
    private AnsibleClient ansibleClient;

    @InjectMocks
    private AnsibleService ansibleService;

    @Test
    void testExecuteJobTemplate_Success() throws AnsibleServiceException {
        // Arrange
        String jobTemplateId = "42";
        String expectedResponse = "Job executed successfully";

        // Mock the behavior of ansibleClient
        when(ansibleClient.launchJobTemplate(jobTemplateId)).thenReturn(expectedResponse);

        // Act
        String actualResponse = ansibleService.executeJobTemplate(jobTemplateId);

        // Assert
        assertEquals(expectedResponse, actualResponse);
        verify(ansibleClient).launchJobTemplate(jobTemplateId); // Verify that the method was called
    }

    @Test
    void testExecuteJobTemplate_Failure() {
        // Arrange
        String jobTemplateId = "42";
        when(ansibleClient.launchJobTemplate(jobTemplateId)).thenThrow(new RuntimeException("Error"));

        // Act and Assert
        AnsibleServiceException exception = assertThrows(AnsibleServiceException.class, () -> ansibleService.executeJobTemplate(jobTemplateId));

        assertTrue(exception.getMessage().contains("Error al ejecutar el job template con el ID: 42"));
        verify(ansibleClient).launchJobTemplate(jobTemplateId); // Verify that the method was called
    }

    @Test
    void testGetJobTemplateDetails_Success() throws AnsibleServiceException {
        // Arrange
        String jobTemplateId = "42";
        String expectedResponse = "Job Template Details";

        // Mock the behavior of ansibleClient
        when(ansibleClient.getJobTemplateDetails(jobTemplateId)).thenReturn(expectedResponse);

        // Act
        String actualResponse = ansibleService.getJobTemplateDetails(jobTemplateId);

        // Assert
        assertEquals(expectedResponse, actualResponse);
        verify(ansibleClient).getJobTemplateDetails(jobTemplateId); // Verify that the method was called
    }

    @Test
    void testGetJobTemplateDetails_Failure() {
        // Arrange
        String jobTemplateId = "42";
        when(ansibleClient.getJobTemplateDetails(jobTemplateId)).thenThrow(new RuntimeException("Error"));

        // Act and Assert
        AnsibleServiceException exception = assertThrows(AnsibleServiceException.class, () -> ansibleService.getJobTemplateDetails(jobTemplateId));

        assertTrue(exception.getMessage().contains("Error al obtener los detalles del job template con el ID: 42"));
        verify(ansibleClient).getJobTemplateDetails(jobTemplateId);
    }

    @Test
    void testGetJobOutput_Success() throws AnsibleServiceException {
        // Arrange
        String jobId = "42";
        String expectedOutput = "Job output details";

        // Mock the behavior of ansibleClient
        when(ansibleClient.getJobOutput(jobId)).thenReturn(expectedOutput);

        // Act
        String actualOutput = ansibleService.getJobOutput(jobId);

        // Assert
        assertEquals(expectedOutput, actualOutput);
        verify(ansibleClient).getJobOutput(jobId); // Verify that the method was called
    }

    @Test
    void testGetJobOutput_Failure() {
        // Arrange
        String jobId = "42";
        when(ansibleClient.getJobOutput(jobId)).thenThrow(new RuntimeException("Error"));

        // Act and Assert
        AnsibleServiceException exception = assertThrows(AnsibleServiceException.class, () -> ansibleService.getJobOutput(jobId));

        assertTrue(exception.getMessage().contains("Error al obtener la salida del trabajo con el ID: 42"));
        verify(ansibleClient).getJobOutput(jobId); // Verify that the method was called
    }

    @Test
    void testGetJobStatus_Success() throws AnsibleServiceException {
        // Arrange
        String jobId = "42";
        String expectedResponse = "{\"status\":\"success\"}"; // Simulando una respuesta JSON
        when(ansibleClient.getJobDetails(jobId)).thenReturn(expectedResponse);

        // Act
        String actualStatus = ansibleService.getJobStatus(jobId);

        // Assert
        assertEquals("success", actualStatus);
        verify(ansibleClient).getJobDetails(jobId); // Verifica que el método fue llamado
    }

    @Test
    void testGetJobStatus_Failure() {
        // Arrange
        String jobId = "42";
        when(ansibleClient.getJobDetails(jobId)).thenThrow(new RuntimeException("Error"));

        // Act and Assert
        AnsibleServiceException exception = assertThrows(AnsibleServiceException.class, () -> ansibleService.getJobStatus(jobId));

        assertTrue(exception.getMessage().contains("Error al obtener el estado del trabajo con ID: 42"));
        verify(ansibleClient).getJobDetails(jobId); // Verifica que el método fue llamado
    }

    @Test
    void testGetHostsFromGroup_Success() throws AnsibleServiceException {
        // Arrange
        Long groupId = 1L;
        String expectedResponse = "Host1, Host2, Host3"; // Simulando una respuesta de hosts

        // Mock the behavior of ansibleClient
        when(ansibleClient.getHostsFromGroup(groupId)).thenReturn(expectedResponse);

        // Act
        String actualResponse = ansibleService.getHostsFromGroup(groupId);

        // Assert
        assertEquals(expectedResponse, actualResponse);
        verify(ansibleClient).getHostsFromGroup(groupId); // Verifica que el método fue llamado
    }

    @Test
    void testGetHostsFromGroup_Failure() {
        // Arrange
        Long groupId = 1L;
        when(ansibleClient.getHostsFromGroup(groupId)).thenThrow(new RuntimeException("Error"));

        // Act and Assert
        AnsibleServiceException exception = assertThrows(AnsibleServiceException.class, () -> ansibleService.getHostsFromGroup(groupId));

        assertTrue(exception.getMessage().contains("Error al obtener los hosts del grupo con ID: 1"));
        verify(ansibleClient).getHostsFromGroup(groupId); // Verifica que el método fue llamado
    }

    @Test
    void testCancelJob_Success() throws AnsibleServiceException {
        // Arrange
        String jobId = "42";
        String expectedResponse = "Job canceled successfully";

        // Mock the behavior of ansibleClient
        when(ansibleClient.cancelJob(jobId)).thenReturn(expectedResponse);

        // Act
        ansibleService.cancelJob(jobId);

        // Assert
        verify(ansibleClient).cancelJob(jobId); // Verifica que el método fue llamado
        // Puedes agregar más verificaciones si es necesario
    }

    @Test
    void testCancelJob_Failure() {
        // Arrange
        String jobId = "42";
        when(ansibleClient.cancelJob(jobId)).thenThrow(new RuntimeException("Error"));

        // Act and Assert
        AnsibleServiceException exception = assertThrows(AnsibleServiceException.class, () -> ansibleService.cancelJob(jobId));

        assertTrue(exception.getMessage().contains("Error al cancelar el trabajo en Ansible: " + jobId));
        verify(ansibleClient).cancelJob(jobId); // Verifica que el método fue llamado
    }
}